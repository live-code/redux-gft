import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../App';

export const languageStore = createSlice({
  name: 'language',
  initialState: { value: 'it' },
  reducers: {
    changeLanguage(state, action: PayloadAction<'it' | 'en'>) {
      return { ...state, value: action.payload }
    }
  }
})

export const { changeLanguage } = languageStore.actions


// selectors
export const getLanguage = (state: RootState) => state.settings.language.value;
export const getThemeName = (state: RootState) => state.settings.theme.value;

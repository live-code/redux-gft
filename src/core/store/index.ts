import { combineReducers } from '@reduxjs/toolkit';
import { languageStore } from './language.store';
import { themeStore } from './theme.store';

export const settingsReducer = combineReducers({
  theme: themeStore.reducer,
  language: languageStore.reducer,
})

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../App';

export const themeStore = createSlice({
  name: 'theme',
  initialState: { value: 'dark' },
  reducers: {
    changeTheme(state, action: PayloadAction<'dark' | 'light'>) {
      return { ...state, value: action.payload }
    }
  }
})

export const { changeTheme } = themeStore.actions


// selectors
export const getTheme = (state: RootState) => state.settings.theme;
export const getThemeName = (state: RootState) => state.settings.theme.value;

import React from 'react';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getCounter } from '../../features/counter/store/counter.selectors';
import { getThemeName } from '../store/theme.store';

const activeStyle: React.CSSProperties = { color: 'orange'};

export const Navbar: React.FC = () => {
  const counter = useSelector(getCounter);
  const theme = useSelector(getThemeName);

  const cls = theme === 'dark' ?
    'navbar navbar-expand navbar-dark bg-dark' :
    'navbar navbar-expand navbar-light bg-light';

  return (
    <nav className={cls}>
      <div className="navbar-brand">
        <NavLink
          style={({ isActive }) =>
            isActive ? activeStyle : {}
          }
          className="nav-link"
          to="/">
          REDUX {theme}
        </NavLink>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/counter">
              <small>counter</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/settings">
              <small>settings</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/users">
              <small>users</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/catalog">
              <small>catalog</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/rtkquery">
              <small>RTK query</small>
            </NavLink>
          </li>

        </ul>
      </div>
    </nav>
  )
}

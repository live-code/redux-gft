import React  from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { changeTheme, getThemeName } from '../../core/store/theme.store';

export const SettingsPage: React.FC = () => {
  const theme = useSelector(getThemeName);
  const dispatch = useDispatch();

  return <div>
    <h2>Settings Page</h2>
    Current theme: {theme}

    <hr/>
    <button onClick={() => dispatch(changeTheme('dark'))}>Change to Dark</button>
    <button onClick={() => dispatch(changeTheme('light'))}>Change to LIght</button>
  </div>
};

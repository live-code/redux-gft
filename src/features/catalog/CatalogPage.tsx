import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addProduct, deleteProduct, getProducts, toggleProduct } from './store/products.actions';
import { RootState } from '../../App';
import { ProductsList } from './components/ProductsList';
import { ProductsForm } from './components/ProductsForm';
import classNames from 'classnames';
import { changeActiveCategory } from './store/categories.store';
import { getCatalogFiltered, getProductError } from './store/products.selectors';

export const CatalogPage: React.FC = () => {
  const dispatch = useDispatch();
  const catalog = useSelector(getCatalogFiltered)
  const categories = useSelector((state: RootState) => state.catalog.categories)
  const error = useSelector(getProductError);

  useEffect(() => {
    dispatch(getProducts())
  }, [dispatch])

  return <div>
    CatalogPage

    {error && <div className="alert alert-danger">Errrore!!!!</div>}

    <ProductsForm
      categories={categories.list}
      addProduct={(formData) => dispatch(addProduct(formData))}
    />

    <hr/>


    // NEW: FILTER CATEGORY
    <button onClick={() => dispatch(changeActiveCategory(-1))}>All</button>
    {
      categories.list.map(c => {
        return (
          <button
            key={c.id}
            className={classNames(
              'btn',
              { 'btn-primary': c.id === categories.active },
              { 'btn-outline-primary': c.id !== categories.active }
            )}
            onClick={() => dispatch(changeActiveCategory(c.id))}
          >
            {c.name}
          </button>
        )
      })
    }

    <ProductsList
      products={catalog}
      onDelete={(id) => dispatch(deleteProduct(id))}
      onToggle={(p) => dispatch(toggleProduct(p))}
    />
  </div>
}

import React, { useState } from 'react';
import { Product } from '../model/product';
import classnames from 'classnames';

interface ProductsFormProps {
  addProduct: (product: Partial<Product>) => void;
  categories: any[];
}

const INITIAL_STATE = { title: '', price: 0, categoryId: -1};

export const ProductsForm: React.FC<ProductsFormProps> = (props) => {
  const [formData, setFormData] = useState<Partial<Product>>(INITIAL_STATE);

  const onChangeHandler = (e: React.FormEvent<HTMLInputElement | HTMLSelectElement>) => {
    console.log(e.currentTarget.type)
    const target = e.currentTarget;

    setFormData({
      ...formData,
      [target.name]: target.type === 'select-one' ? +target.value : target.value
    })
  };

  const onSubmitHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    props.addProduct(formData);
    setFormData(INITIAL_STATE);
  };

  const isTitleValid = formData.title && formData.title.length > 3;
  const isPriceValid = formData.price && formData.price > 0;
  const isCategoryValid = formData.categoryId && formData.categoryId !== -1;
  const isFormValid = isTitleValid && isPriceValid && isCategoryValid;

  return <div>
    <form onSubmit={onSubmitHandler}>
      <input
        className={classnames(
          'form-control',
          { 'is-valid': isTitleValid },
          { 'is-invalid': !isTitleValid },
        )}
        type="text"
        placeholder="Write title..."
        onChange={onChangeHandler}
        value={formData.title}
        name="title"
      />

      <input
        className={classnames(
          'form-control',
          { 'is-valid': isPriceValid },
          { 'is-invalid': !isPriceValid },
        )}
        type="text"
        placeholder="Write price..."
        onChange={onChangeHandler}
        value={formData.price}
        name="price"
      />

      <select
        className={classnames(
          'form-control',
          { 'is-valid': isCategoryValid },
          { 'is-invalid': !isCategoryValid },
        )}
        name="categoryId"
        value={formData.categoryId}
        onChange={onChangeHandler}
      >
        <option value={-1}>Select a category</option>
        {
          props.categories.map(c => {
            return <option key={c.id} value={c.id}>{c.name}</option>
          })
        }
      </select>

      <button type="submit" disabled={!isFormValid}>SAVE</button>
    </form>
  </div>
};

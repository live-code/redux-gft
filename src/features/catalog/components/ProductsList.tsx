import React  from 'react';
import { Product } from '../model/product';
import { deleteProduct, toggleProduct } from '../store/products.actions';

interface ProductsListProps {
  products: Product[];
  onDelete: (id: number) => void;
  onToggle: (product: Product) => void;
}


export const ProductsList: React.VFC<ProductsListProps> = ({
  onDelete, onToggle, products
}) => {

  return <>
    {
      products.map(p => {
        return (
          <li key={p.id} style={{ opacity: p.visibility ? 1 : 0.4}}>
            {p.title} - {p.categoryId}

            <i
              className="fa fa-trash"
              onClick={() => onDelete(p.id)}
            />

            <i
              className="fa fa-eye"
              onClick={() => onToggle(p)}
            />
          </li>
        )
      })
    }
  </>
};

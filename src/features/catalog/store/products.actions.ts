import { AppThunk } from '../../../App';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import {
  addProductSuccess,
  deleteProductSuccess,
  getProductsSuccess,
  setError,
  toggleProductVisibilitySuccess
} from './products.store';
import { Product } from '../model/product';

export const getProducts = (): AppThunk => async (dispatch) => {
  try {
    const response = await axios.get<Product[]>('http://localhost:3001/products');
    dispatch(getProductsSuccess(response.data))
  } catch (err) {
    dispatch(setError())

  }
}

export const deleteProduct = (id: number): AppThunk => async dispatch => {
  try {
    await axios.delete(`http://localhost:3001/products/${id}`);
    dispatch(deleteProductSuccess(id))
  } catch (err) {
    dispatch(setError())
  }
};

export const getProducts2 = function(): AppThunk {
  return async function(dispatch) {
    try {
      const response = await axios.get<Product[]>('http://localhost:3001/products');
      dispatch(getProductsSuccess(response.data))
    } catch (err) {
      dispatch(setError())
    }
  }
}

export const addProduct = (
  product: Partial<Product>
): AppThunk => async dispatch => {
  try {
    const newProduct = await axios.post<Product>('http://localhost:3001/products', {
        ...product,
        visibility: false
      }
    );
    dispatch(addProductSuccess(newProduct.data))
  } catch (err) {
    dispatch(setError())
  }
};
export const toggleProduct = (product: Product): AppThunk => async dispatch => {
  try {
    const updatedProduct: Product = {
      ...product,
      visibility: !product.visibility
    };
    const response = await axios.patch<Product>(`http://localhost:3001/products/${product.id}`, updatedProduct);
    dispatch(toggleProductVisibilitySuccess(response.data))
  } catch (err) {
    // dispatch error
  }
};

import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Product } from '../model/product';

export const productsStore = createSlice({
  name: 'products',
  initialState: {
    list:  [] as Product[],
    error: false,
  },
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      state.error = false;
      return {
        ...state,
        list: action.payload
      };
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.error = false;
      state.list.push(action.payload)
      // return [...state, action.payload]
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      state.error = false;
      const index = state.list.findIndex(p => p.id === action.payload)
      state.list.splice(index, 1)
      // return state.filter(p => p.id !== action.payload)
    },
    toggleProductVisibilitySuccess(state, action: PayloadAction<Product>) {
      state.error = false;
      const product = state.list.find(p => p.id === action.payload.id);
      if (product) {
        product.visibility = action.payload.visibility;
      }
    },
    setError(state) {
      state.error = true;
    }
  }
});

export const {
  setError, addProductSuccess, getProductsSuccess, toggleProductVisibilitySuccess, deleteProductSuccess
} = productsStore.actions;

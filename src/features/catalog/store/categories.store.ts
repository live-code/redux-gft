// features/catalog/store/categories.store.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../App';

export const categoriesStore = createSlice({
  name: '[products categories]',
  initialState: {
    list : [
      { id: 1, name: 'Latticini'},
      { id: 2, name: 'Frutta'},
    ],
    active: -1
  },
  reducers: {
    changeActiveCategory(state, action: PayloadAction<number>) {
      state.active = action.payload;
    }
  }
})

export const { changeActiveCategory } = categoriesStore.actions



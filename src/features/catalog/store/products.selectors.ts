import { RootState } from '../../../App';

export const getProductError = (state: RootState) =>
  state.catalog.products.error

export const getCatalogFiltered = (state: RootState) => state.catalog.products.list.filter(p => {
  const active = state.catalog.categories.active;
  return active  === -1 || p.categoryId === active
})

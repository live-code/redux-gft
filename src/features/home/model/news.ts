export interface News {
  id: number;
  title: string;
  published: boolean;
}

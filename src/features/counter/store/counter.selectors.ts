import { RootState } from '../../../App';


export const getCounter = (state: RootState) => state.shipping.counter
export const getTotalPallet = (state: RootState) => state.shipping.counter / state.shipping.config.itemsPerPallet;

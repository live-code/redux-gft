import { RootState } from '../../../App';

export const getMaterial = (state: RootState) => state.shipping.config.material;
export const getItemPerPallets = (state: RootState) => {
  return state.shipping.config.itemsPerPallet;
}

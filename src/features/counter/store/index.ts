import { combineReducers } from '@reduxjs/toolkit';
import { counterReducer } from './counter.reducer';
import { configReducer } from './config.reducer';

export const shippingReducer = combineReducers({
  counter: counterReducer,
  config: configReducer
})

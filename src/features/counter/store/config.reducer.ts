import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { Config } from '../model/config';
import { setConfig } from './config.actions';

const INITIAL_STATE: Config = {
  itemsPerPallet: 5,
  material: 'wood'
}
export const configReducer = createReducer<Config>(INITIAL_STATE, {
  [setConfig.type]: (state: Config, action: PayloadAction<Config>) => {
    return { ...state, ...action.payload }
  }
})

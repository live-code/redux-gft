import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { decrement, increment, reset } from './store/counter.actions';
import { getCounter, getTotalPallet } from './store/counter.selectors';
import { setConfig } from './store/config.actions';
import { getItemPerPallets, getMaterial } from './store/config.selectors';

export const CounterPage: React.FC = () => {
  const counter = useSelector(getCounter);
  const totalPallets = useSelector(getTotalPallet);
  const material = useSelector(getMaterial);
  const itemPerPallet = useSelector(getItemPerPallets);

  const dispatch = useDispatch();

  return <div>
    <div>N° products: {counter}</div>
    <div>N° pallets: {totalPallets} ({itemPerPallet} per pallet)</div>
    <div>Material: {material}</div>
    <hr/>
    <button onClick={() => dispatch(decrement(10))}>-</button>
    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(reset())}>RESET</button>
    <hr/>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 5}))}>PALLET: 5</button>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 10}))}>PALLET: 10</button>
    <button onClick={() => dispatch(setConfig({ material: 'wood'}))}>WOOD</button>
    <button onClick={() => dispatch(setConfig({ material: 'plastic'}))}>Plastic</button>
  </div>
}


import React  from 'react';
import { useGetUserByIdQuery } from '../api/users';
import { useParams } from 'react-router-dom';

export const Demo1Details: React.FC = () => {
  const { id = '' } = useParams();
  const { data, refetch: refetchUser } = useGetUserByIdQuery(+id)

  return <pre>{JSON.stringify(data, null, 4)}</pre>
};

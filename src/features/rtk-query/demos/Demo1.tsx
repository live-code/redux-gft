import React  from 'react';
import { useGetUserByIdQuery, useGetUsersQuery, usePrefetch } from '../api/users';
import { useNavigate } from 'react-router-dom';

// COMPONENT
export const Demo1: React.FC = () => {
  const { data: users, error, isLoading, isFetching, isSuccess, refetch } = useGetUsersQuery();
  const prefetchUser = usePrefetch('getUserById')
  const nagigate = useNavigate();

  function showDetails(id: number) {
    console.log(id)
    nagigate('/rtkquery/' + id);
  }

  return <div>
    <button onClick={refetch}>refetch</button>
    <div>isLoading: {JSON.stringify(isLoading)}</div>
    <div>isFetching: {JSON.stringify(isFetching)}</div>
    <pre>error{JSON.stringify(error, null, 2)}</pre>
    {
      users?.map(u => {
        return (
          <li
            key={u.id}
            onMouseOver={() => prefetchUser(u.id)}
            onClick={() => showDetails(u.id)}
          >{u.name}</li>
        )
      })
    }

  </div>
};

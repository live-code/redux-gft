import React  from 'react';
import { NavLink, Outlet, useLocation, useNavigate, useParams } from 'react-router-dom';

export const RTKqueryPage: React.FC = () => {
  const navigate = useNavigate()
  // const params = useParams();
  const location = useLocation();

  function goto() {
    navigate('demo2')
  }

  return <div>
    <NavLink to="./">Demo1</NavLink>
    <NavLink to="demo2">Demo2</NavLink>
    <NavLink to="demo3">Demo3</NavLink>

    <button onClick={goto}>Demo2</button>
    <hr/>

    <Outlet />
  </div>
};

// USER API
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

interface User {
  id: number;
  name: string;
  email: string;
  phone: string;
}

export const usersAPI = createApi({
  reducerPath: 'users',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://jsonplaceholder.typicode.com'}),
  // refetchOnMountOrArgChange: true,
  keepUnusedDataFor: 5,
  endpoints: (builder) => ({
    getUsers: builder.query<Pick<User, 'id' | 'name'>[], void>({
      query: () => 'users',
      /*transformResponse(baseQueryReturnValue: BaseQueryResult<BaseQuery>, meta: BaseQueryMeta<BaseQuery>): Promise<ResultType> | ResultType {
      }*/
      transformResponse: (response: User[]): Pick<User, 'id' | 'name'>[] => response.map(u => {
        return { id: u.id, name: u.name}
      }),
      // keepUnusedDataFor: 3,
    }),
    getUserById: builder.query<User, number>({
      query: (id) => `users/${id}`
    }),
  })
})

export const { useGetUsersQuery, useGetUserByIdQuery, usePrefetch } = usersAPI;

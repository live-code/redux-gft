import React from 'react';
import './App.css';
import { BrowserRouter, Navigate, Route, Routes, useRoutes } from 'react-router-dom';
import { HomePage } from './features/home/HomePage';
import { CounterPage } from './features/counter/CounterPage';
import { UsersPage } from './features/users/UsersPage';
import { CatalogPage } from './features/catalog/CatalogPage';
import { Navbar } from './core/components/Navbar';
import { Action, combineReducers, configureStore, getDefaultMiddleware, ThunkAction } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { counterReducer } from './features/counter/store/counter.reducer';
import { configReducer } from './features/counter/store/config.reducer';
import { shippingReducer } from './features/counter/store';
import { newsStore } from './features/home/store/news.store';
import { productsStore } from './features/catalog/store/products.store';
import { themeStore } from './core/store/theme.store';
import { SettingsPage } from './features/settings/SettingsPage';
import { languageStore } from './core/store/language.store';
import { settingsReducer } from './core/store';
import { categoriesStore } from './features/catalog/store/categories.store';
import { catalogReducers } from './features/catalog/store';
import { RTKqueryPage } from './features/rtk-query/RTKqueryPage';
import { Demo1 } from './features/rtk-query/demos/Demo1';
import { Demo2 } from './features/rtk-query/demos/Demo2';
import { Demo3 } from './features/rtk-query/demos/Demo3';
import { usersAPI } from './features/rtk-query/api/users';
import { Demo1Details } from './features/rtk-query/demos/Demo1Details';


const rootReducer = combineReducers({
  shipping: shippingReducer,
  news: newsStore.reducer,
  catalog: catalogReducers,
  settings: settingsReducer,
  [usersAPI.reducerPath]: usersAPI.reducer
})
export const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(
    usersAPI.middleware
  )
})

export type RootState = ReturnType<typeof rootReducer>
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>

const MyRoutes: React.VFC = () => {
  const routes = useRoutes([
    { path: '/home', element: <HomePage /> },
    { path: '/counter', element: <CounterPage /> },
    { path: '/users', element: <UsersPage /> },
    { path: '/catalog', element: <CatalogPage /> },
    {
      path: '/rtkquery',
      element: <RTKqueryPage />,
      children: [
        { index: true, element: <Demo1 /> },
        { path: ':id', element: <Demo1Details /> },
        { path: 'demo2', element: <Demo2 /> },
        { path: 'demo3', element: <Demo3 /> },
      ]
    },
    { path: '*', element:  <Navigate to="/home" /> },
  ])
  return routes;
}


function App() {

  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />
        <MyRoutes />
      </BrowserRouter>
    </Provider>
  );
}

export default App;

import React from 'react';
import './App.css';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { HomePage } from './features/home/HomePage';
import { CounterPage } from './features/counter/CounterPage';
import { UsersPage } from './features/users/UsersPage';
import { CatalogPage } from './features/catalog/CatalogPage';
import { Navbar } from './core/components/Navbar';
import { Action, combineReducers, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { counterReducer } from './features/counter/store/counter.reducer';
import { configReducer } from './features/counter/store/config.reducer';
import { shippingReducer } from './features/counter/store';
import { newsStore } from './features/home/store/news.store';
import { productsStore } from './features/catalog/store/products.store';
import { themeStore } from './core/store/theme.store';
import { SettingsPage } from './features/settings/SettingsPage';
import { languageStore } from './core/store/language.store';
import { settingsReducer } from './core/store';
import { categoriesStore } from './features/catalog/store/categories.store';
import { catalogReducers } from './features/catalog/store';
import { RTKqueryPage } from './features/rtk-query/RTKqueryPage';
import { Demo1 } from './features/rtk-query/demos/Demo1';
import { Demo2 } from './features/rtk-query/demos/Demo2';
import { Demo3 } from './features/rtk-query/demos/Demo3';


const rootReducer = combineReducers({
  shipping: shippingReducer,
  news: newsStore.reducer,
  catalog: catalogReducers,
  settings: settingsReducer,
})
export const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production'
})

export type RootState = ReturnType<typeof rootReducer>
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>

function App() {

  return (
    <Provider store={store}>

      <BrowserRouter>

        <Navbar />

        <Routes>
          <Route path="/home" element={<HomePage />} />
          <Route path="/counter" element={<CounterPage />} />
          <Route path="/users" element={<UsersPage />} />
          <Route path="/catalog" element={<CatalogPage />} />
          <Route path="/settings" element={<SettingsPage />} />
          <Route path="/rtkquery" element={<RTKqueryPage />}>
            <Route index element={<Demo1 />} />
            <Route path="demo2"  element={<Demo2 />} />
            <Route path="demo3"  element={<Demo3 />} />
          </Route>
          <Route
            path="*"
            element={
              <Navigate to="/home" />
            }
          />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
